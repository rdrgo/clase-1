<?php 

// Declarar variables

    $auxiliar = 10;
    $apellido = 'Soto';
    $arreglo = ['lunes','martes','miercoles','jueves','viernes'];

// Imprimir en pantalla

    echo "El valor de la variable auxiliar es: ".$auxiliar."<br>";
    echo "El apellido es: $apellido <br>";
    echo "La variable auxiliar es de tipo: ".gettype($auxiliar)."<br>";
    //echo $arreglo;

//Imprimir el arreglo 

    echo '<br>';
    echo "Recorriendo arreglo <br>";
        for($i=0; $i<sizeof($arreglo); $i++){
            echo $arreglo[$i].'<br>';
        }
    echo '<br>';

//Concatenar strings

    echo $dia1 = 'Hoy es '.$arreglo[2].' y estamos en clase. <br>';
    echo $dia2 = "AYER ERA $arreglo[1] Y TAMBIEN TUVIMOS CLASE. <br>";

    $dia1 = strtoupper($dia1);
    $dia2 = strtolower($dia2);

    echo $dia1.'<br>';
    echo $dia2.'<br>';

//Funciones definidas

    $cuenta = 200;

    if(empty($cuenta)){
        echo 'La cuenta está vacía.';
    }else{
        echo 'La cuenta tiene '.$cuenta.' pesos <br>';
    }

    //calcular tamaño string
    $contrasena = 'hola123';
    echo 'El string contraseña tiene un largo de: '.strlen($contrasena).' carácteres <br>';

    //conocer el tipo de variable
    echo 'El tipo de variable contrasena es: '.gettype($contrasena).'<br>';

//Escribir funciones 

    echo '<strong>Funciones:</strong> <br>';

    $numero1 = 4;
    $numero2 = 5;

    echo 'La suma es: '.sumar($numero1,$numero2).'<br>';
    echo 'La resta es: '.restar($numero1,$numero2).'<br>';
    multiplicar($numero1,$numero2);
    dividir($numero1,$numero2);


    

    function sumar($a, $b){
        return $a+$b;
    }

    function restar($a, $b){
        return $a-$b;
    }

    function multiplicar($a, $b){
        echo 'La multiplicación es: '.$a*$b.'<br>';
    }

    function dividir($a, $b){
        if($b!=0){
            echo 'La división es: '.$a/$b.'<br>';
        }else{
            echo 'No se puede dividir por cero';
        }
    }


?>